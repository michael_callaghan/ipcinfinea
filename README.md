infinea-cordova-plugin
========================

Created by Brad Schuran
Infinite Peripherals

Tested on Cordova ver.3.3

(c) Infinite Peripherals
http://ipcprint.com


Cordova CLI Installation Instructions:

cordova create cordovaTest com.test.test
cd cordovaTest
cordova plugin add https://bradipcprint@bitbucket.org/bradipcprint/ipcinfinea.git
cordova platform add ios
cordova build iOS 