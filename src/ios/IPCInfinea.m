#import "IPCInfinea.h"

@implementation Infinea

- (void)pluginInitialize
{
}

-(void)connectionState:(int)state {
    

    if(state==CONN_CONNECTED)
    {
        [sdk barcodeSetTypeMode:BARCODE_TYPE_EXTENDED error:nil];
        [sdk setAutoOffWhenIdle:36000 whenDisconnected:36000 error:nil];
    }else
    {
    }
    
    NSString *func=[NSString stringWithFormat:@"%@(%@);",[cb valueForKey:@"barcodeStatusCallback"],state==CONN_CONNECTED?@"true":@"false"];
    
    [[super webView] stringByEvaluatingJavaScriptFromString:func];
}

-(void)barcodeData:(NSString *)barcode type:(int)type
{
    NSString *func=[NSString stringWithFormat:@"%@(\"%@\",%d,\"%@\");",[cb valueForKey:@"barcodeDataCallback"],barcode,type,[sdk barcodeType2Text:type]];
    [[super webView] stringByEvaluatingJavaScriptFromString:func];
}

-(void)initWithCallbacks:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    sdk=[DTDevices sharedDevice];
    sdk.delegate=self;
    [sdk connect];
    
    @try {
        cb=[[command.arguments objectAtIndex:0] copy];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    } @catch (id exception) {
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

-(void)barScan:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSError *error;
    
    @try {
        NSString* myarg = [[command.arguments objectAtIndex:0] lowercaseString];
        bool success=false;
        
        if([myarg isEqualToString:@"on"] || [myarg isEqualToString:@"yes"] || [myarg isEqualToString:@"true"] || [myarg isEqualToString:@"1"])
            success=[sdk barcodeStartScan:&error];
        else
            success=[sdk barcodeStopScan:&error];
        
        if (success) {
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }else
        {
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
        
    } @catch (id exception) {
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

-(void)barSetScanMode:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSError *error;
    
    @try {
        bool success=false;
        
        success=[sdk barcodeSetScanMode:[[command.arguments objectAtIndex:0] intValue] error:&error];
        
        if (success) {
           [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }else
        {
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
        
    } @catch (id exception) {
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

-(void)barOpticonSetCustomConfig:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSError *error;
    
    @try {
        bool success=false;
        
        success=[sdk barcodeOpticonSetInitString:[command.arguments objectAtIndex:0] error:&error];
        
        if (success) {
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }else
        {
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
        
    } @catch (id exception) {
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

-(void)barIntermecSetCustomConfig:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSError *error;
    
    @try {
        bool success=false;
        NSArray *data=[command.arguments objectAtIndex:0];
        uint8_t buf[data.count];
        for(int i=0;i<data.count;i++)
            buf[i]=(uint8_t)[[data objectAtIndex:i] intValue];
        success=[sdk barcodeIntermecSetInitData:[NSData dataWithBytes:buf length:sizeof(buf)] error:&error];
        
        if (success) {
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }else
        {
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
        
    } @catch (id exception) {
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

-(void)barCodeSetParams:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSError *error;
    
    @try {
        bool success=false;
        NSArray *data=[command.arguments objectAtIndex:0];
        
        for(int i=0;i<data.count;i+=2)
        {
            success=[sdk barcodeCodeSetParam:[[data objectAtIndex:i] intValue] value:[[data objectAtIndex:i+1] intValue] error:&error];
            if(!success)
                break;
        }
        
        if (success) {
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }else
        {
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
        
    } @catch (id exception) {
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

@end
