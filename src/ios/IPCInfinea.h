#import <Cordova/CDVPlugin.h>
#import "DTDevices.h"


@interface Infinea : CDVPlugin
{
    NSDictionary *cb;
    DTDevices *sdk;
}

-(void)pluginInitialize;
-(void)initWithCallbacks:(CDVInvokedUrlCommand*)command;
-(void)barScan:(CDVInvokedUrlCommand*)command;
-(void)barSetScanMode:(CDVInvokedUrlCommand*)command;
-(void)barOpticonSetCustomConfig:(CDVInvokedUrlCommand*)command;
-(void)barIntermecSetCustomConfig:(CDVInvokedUrlCommand*)command;
-(void)barCodeSetParams:(CDVInvokedUrlCommand*)command;
@end
